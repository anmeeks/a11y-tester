$(document).ready(function () {
    // Makes it so labels are unique for each chart
    Highcharts.setOptions({
        lang: {
            accessibility: {
                screenReaderSection: {
                    beforeRegionLabel: 'Chart screen reader information, {chart.options.title.text}'
                },
                exporting: {
                    exportRegionLabel: "Chart menu, {chart.options.title.text}"
                }
            }
        }
    });

    var socket = io('//' + document.location.hostname + ':' + document.location.port);
    let currentScan, status;

    const showMessage = msg => {
        $('#messages').append(`<p class="alert alert-info">${msg}</p>`).find('p:last-child').delay(3000).fadeTo(400, 0, function () { this.remove(); });
    }
    const error = msg => {
        $('#alerts').append(`<p class="alert alert-danger">${msg}</p>`).find('p:last-child').delay(3000).fadeTo(400, 0, function () { this.remove(); });
        console.error('Error');
        console.error(msg);
    }
    socket.on('error', msg => {
        error(msg);
    });

    let segments = window.location.href.split('/');
    currentScan = segments[segments.length - 1];

    $.getJSON('/reports/wcagFailures/' + currentScan, function (data) {
        let wcagConf = getWcagConf(data);
        Highcharts.chart('wcag', wcagConf);
    });



    function messageFromRelatedNodes(relatedNodes) {
        var retVal = '';
        if (relatedNodes.length) {
            var list = relatedNodes.map(function (node) {
                return {
                    targetArrayString: JSON.stringify(node.target),

                    targetString: compiledTargetsTemplate({
                        targets: node.target
                    }),
                    htmlString: compiledCodeTemplate({
                        html: Prism.highlight(node.html, Prism.languages.html, 'html')
                    })
                };
            });
            retVal += compiledRelatedListTemplate({ relatedNodeList: list });
        }
        return retVal;
    }

    function messagesFromArray(nodes) {
        var list = nodes.map(function (failure) {
            return {
                message: failure.message,// failure.message.replace(/</gi, '&lt;').replace(/>/gi, '&gt;'),
                relatedNodesMessage: messageFromRelatedNodes(failure.relatedNodes)
            }
        });
        return compiledReasonsTemplate({ reasonsList: list });
    }

    function summary(node, fix) {
        var toggle = `
            <span class="fas fa-plus-square" aria-hidden="true"></span><span class="fas fa-minus-square" aria-hidden="true"></span> `
        var retVal = `
            <button 
                class="button button-primary" 
                type="button" data-toggle="collapse" 
                data-target="#${fix}" 
                aria-expanded="false" 
                aria-controls="${fix}">
                    ${toggle}How to fix
            </button> 
            <div class="collapse" id="${fix}">`;
        if (node.any.length) {
            retVal += '<h6 class="error-title">Fix any of the following</h6>';
            retVal += messagesFromArray(node.any);
        }

        var all = node.all.concat(node.none);
        if (all.length) {
            retVal += '<h6 class="error-title">Fix all of the following</h6>';
            retVal += messagesFromArray(all);
        }
        retVal += "</div>"
        return retVal;
    }

    function initPanel(evt) {
        let controls = $(evt.target).attr('aria-controls');
        let i = controls.replace("collapse", "");

        let params = { scanId: currentScan, viewNum: i }
        socket.emit('getViewViolationLength', params);
        socket.emit('getViewViolationInstancesLength', params);
        socket.emit('getViewStats', params);
        socket.emit('getViolations', params);

        $(evt.target).off("." + controls);
    }
    socket.on('viewViolationLength', function (data) {
        let i = data.viewNum;
        let violations = data.violations;
        Highcharts.chart('violationsChart' + i, getViolationsConf(violations));
        console.log("violations length");
    });
    socket.on('viewViolationInstancesLength', function (data) {
        let i = data.viewNum;
        let instances = data.instances;
        Highcharts.chart('issuesChart' + i, getIssuesConf(instances));
        console.log("instances length");
    });
    socket.on('viewStats', function (data) {
        let i = data.viewNum;
        let panelSelector = "#panel" + i;
        let pageTabPanelSelector = panelSelector + " #collapse" + i;
        let pageBodySelector = pageTabPanelSelector + " .panel-body";
        var stats = compiledStatsTemplate(data);
        $(pageBodySelector).prepend(stats);
        console.log("instances stats");
    });
    socket.on('violations', function (data) {
        console.log(data);
        let i = data.viewNum;
        let violations = data.violations;
        let panelSelector = "#panel" + i;
        let pageTabPanelSelector = panelSelector + " #collapse" + i;
        let pageBodySelector = pageTabPanelSelector + " .panel-body";
        let violationsSelector = pageBodySelector + " .violations";
        let parsedViolations = violations.map(function (rule, v) {

            if (rule.impact === 'critical') {
                variation = 'danger';
                icon = 'skull-crossbones';
            } else if (rule.impact === 'serious') {
                variation = 'warning';
                icon = 'radiation-alt';
            } else if (rule.impact === 'moderate') {
                variation = 'info';
                icon = 'exclamation-triangle';
            } else if (rule.impact === 'minor') {
                variation = 'success';
                icon = 'bell';
            }

            return {
                impact: rule.impact,
                variation: variation,
                icon: icon,
                help: rule.help,
                description: rule.description,
                helpUrl: rule.helpUrl,
                count: rule.nodes.length,
                index: v,
                nodes: rule.nodes.map((node, n) => {

                    ({ target, html, failureSummary, any, all, none } = node);

                    html = compiledCodeTemplate({
                        html: Prism.highlight(html, Prism.languages.html, 'html')
                    });
                    target = compiledTargetsTemplate({ targets: target });

                    fixId = "fix" + i + "_" + v + "_" + n
                    reasonHtml = summary(node, fixId);

                    return {
                        target, html, reasonHtml, failureSummary, index: n
                    }
                })
            };

        });

        let myViolations = compiledViolationsTemplate({ violationList: parsedViolations });
        $(violationsSelector).html(myViolations);
    });


    function helperItemIterator(items, template) {
        var out = '';
        if (items) {
            for (var i = 0; i < items.length; i++) {
                out += template(items[i]);
            }
        }
        return out;
    }
    Handlebars.registerHelper('violations', function (items) {
        return helperItemIterator(items, compiledViolationTemplate);
    });
    Handlebars.registerHelper('related', function (items) {
        return helperItemIterator(items, compiledRelatedNodeTemplate);
    });
    Handlebars.registerHelper('reasons', function (items) {
        return helperItemIterator(items, compiledFailureTemplate);
    });
    Handlebars.registerHelper('insts', function (items) {
        return helperItemIterator(items, compiledInstTemplate);
    });
    Handlebars.registerPartial("instances", instancesTemplate.innerHTML);

    //compiledSiteStatsTemplate = Handlebars.templates.siteStats;
    compiledTargetsTemplate = Handlebars.templates.targets;
    compiledCodeTemplate = Handlebars.templates.code;

    compiledViolationTemplate = Handlebars.templates.violation;
    compiledViolationsTemplate = Handlebars.templates.violations;

    compiledPagePanelTemplate = Handlebars.templates.pagePanel;
    compiledPageTabPanelTemplate = Handlebars.templates.pageTabPanel;
    compiledPageHeadTemplate = Handlebars.templates.pageHead;
    compiledChartsTemplate = Handlebars.templates.charts;
    compiledPageBodyTemplate = Handlebars.templates.pageBody;
    compiledRulePanelTemplate = Handlebars.templates.rulePanel;

    compiledRelatedListTemplate = Handlebars.templates.relatedList;
    compiledRelatedNodeTemplate = Handlebars.templates.relatedNode;
    compiledFailureTemplate = Handlebars.templates.failure;
    compiledReasonsTemplate = Handlebars.templates.reasons;

    compiledStatsTemplate = Handlebars.templates.stats;
    $('#accordion > .panel > .panel-heading > h4 > button').each(function (index) {
        console.log(index);
        $(this).on("click.collapse" + index, initPanel);
    });
})
