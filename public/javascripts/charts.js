const getViolationsConf = ({
    critical = 0,
    serious = 0,
    moderate = 0,
    minor = 0
}) => {
    let violationsConf = {
        chart: { type: 'pie', animation: false },
        title: { text: 'Rules Violated' },

        subtitle: { text: '# rules violated by impact category' },

        tooltip: { borderColor: '#8ae' },

        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    connectorColor: '#777',
                    format: '<b>{point.name}</b>: {point.y}'
                },
                cursor: 'pointer',
                borderWidth: 3,
                animation: false
            }
        },

        series: [{
            name: 'Rules Violated ',
            data: []
        }],

        responsive: {
            rules: [{
                condition: { maxWidth: 500 },
                chartOptions: {
                    plotOptions: {
                        series: {
                            dataLabels: { format: '<b>{point.name}</b>' }
                        }
                    }
                }
            }]
        }
    }
    if (critical) {
        violationsConf.series[0].data.push({
            name: 'Critical',
            y: critical,
            color: getColorPattern(0)
        })
    }
    if (serious) {
        violationsConf.series[0].data.push({
            name: 'Serious',
            y: serious,
            color: getColorPattern(1)
        })
    }
    if (moderate) {
        violationsConf.series[0].data.push({
            name: 'Moderate',
            y: moderate,
            color: getColorPattern(2)
        })
    }
    if (minor) {
        violationsConf.series[0].data.push({
            name: 'Minor',
            y: minor,
            color: getColorPattern(3)
        })
    }
    return violationsConf;
}

const getIssuesConf = ({
    critical = 0,
    serious = 0,
    moderate = 0,
    minor = 0
}) => {
    let issuesConf = {
        chart: { type: 'pie', animation: false },
        title: { text: 'Violation Instances' },
        subtitle: { text: '# instances of violations by impact category' },
        tooltip: { borderColor: '#8ae' },

        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    connectorColor: '#777',
                    format: '<b>{point.name}</b>: {point.y}'
                },
                cursor: 'pointer',
                borderWidth: 3,
                animation: false
            }
        },

        series: [{
            name: 'Violation Instances ',
            data: []
        }],

        responsive: {
            rules: [{
                condition: { maxWidth: 500 },
                chartOptions: {
                    plotOptions: {
                        series: {
                            dataLabels: { format: '<b>{point.name}</b>' }
                        }
                    }
                }
            }]
        }
    }
    if (critical) {
        issuesConf.series[0].data.push({
            name: 'Critical',
            y: critical,
            color: getColorPattern(0)
        })
    }
    if (serious) {
        issuesConf.series[0].data.push({
            name: 'Serious',
            y: serious,
            color: getColorPattern(1)
        })
    }
    if (moderate) {
        issuesConf.series[0].data.push({
            name: 'Moderate',
            y: moderate,
            color: getColorPattern(2)
        })
    }
    if (minor) {
        issuesConf.series[0].data.push({
            name: 'Minor',
            y: minor,
            color: getColorPattern(3)
        })
    }
    return issuesConf;
}
const getWcagConf = ({
    categories = [],
    data = [],
    criteria = {}
}) => {
    wcagConf = {
        legend: {
            enabled: false
        },
        tooltip: {
            enabled: true,

            formatter: function () {
                var page = (this.y > 1) ? 'pages' : 'page';
                var violates = (this.y > 1) ? 'violate' : 'violates';
                return this.y + ' ' + page + ' ' + violates + ' <br><b>' + criteria[this.x].name + ' - Level ' + criteria[this.x].level
                '</b>';
            }
        },
        chart: {
            type: 'bar'
        },
        lang: {
            exportData: {
                categoryHeader: 'Success Criteria'
            }
        },
        title: {
            text: 'WCAG Success Criteria Failures'
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            title: {
                text: 'Pages Failed'
            },
            allowDecimals: false
        },
        series: [{
            name: "Pages Failed",
            data: data
        }],
        plotOptions: {
            series: {
                animation: false
            }
        }
    }
    return wcagConf;
}

const getColorPattern = i => {
    // var colors = Highcharts.getOptions().colors,
    var patternColors = ['#EB0000', 'orange', 'purple', 'blue', 'green'],
        patterns = [
            'M 0 0 L 5 5 M 4.5 -0.5 L 5.5 0.5 M -0.5 4.5 L 0.5 5.5',
            'M 0 5 L 5 0 M -0.5 0.5 L 0.5 -0.5 M 4.5 5.5 L 5.5 4.5',
            'M 1.5 0 L 1.5 5 M 4 0 L 4 5',
            'M 0 1.5 L 5 1.5 M 0 4 L 5 4',
            'M 0 1.5 L 2.5 1.5 L 2.5 0 M 2.5 5 L 2.5 3.5 L 5 3.5'
        ];

    return {
        pattern: {
            path: patterns[i],
            color: patternColors[i],
            width: 5,
            height: 5
        }
    };
}