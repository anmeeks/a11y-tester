module.exports = (db) => {
  const fs = require('fs');
  const formatTimecode = require('./timecode');

  let devices, tags, categories;

  let impact = ['critical', 'serious', 'moderate', 'minor'];

  devices = db.getData('/constants/devices/');
  tags = db.getData('/constants/tags/');
  categories = db.getData('/constants/categories/');

  const getCheckedDevices = id => {
    return db.getData(`/reports/${id}/devices/`);
  }
  const getCheckedTags = id => {
    return db.getData(`/reports/${id}/tags/`);
  }
  const getCheckedCategories = id => {
    return db.getData(`/reports/${id}/categories/`);
  }

  const numViews = scanId => {
    let results = db.getData(`/reports/${scanId}/results/`);
    return results.length;
  }

  const numCompleted = scanId => {
    let results = db.getData(`/reports/${scanId}/results/`);
    let i = 0;
    results.forEach(view => {
      if (typeof view.error === 'undefined') {
        i++;
      }
    });
    return i;
  }
  const viewsPassed = scanId => {
    let results = db.getData(`/reports/${scanId}/results/`);
    let passed = 0;
    results.forEach(view => {
      if ((typeof view.error === 'undefined') && (view.violations.length === 0)) passed++;
    });

    return passed;
  }
  const keyExists = (parent, key) => {
    let keys = Object.keys(db.getData(parent));
    return (keys.indexOf(key) !== -1);
  }
  const siteHealth = scanId => {
    try {
      if (keyExists(`/reports/${scanId}`, 'results')) {
        let results = db.getData(`/reports/${scanId}/results/`);
        let total = 0;
        numSuccessful = 0
        results.forEach(item => {
          if (typeof item.error === 'undefined') {
            numSuccessful++;
            let numViolations = item.violations.length;
            let numPasses = item.passes.length;
            let subtotal = numViolations + numPasses;
            let score = (subtotal>0) ? Math.round(numPasses / subtotal * 100) : 100;
            total += score;
          }
        });
        let health = total ? Math.round(total / numSuccessful) : total;

        return health;
      } else {
        return false;
      }

    } catch (e) {
      console.error(e);
      throw Error('Problem getting the site health')
    }
  }

  const getReportName = scanId => {
    if (keyExists(`/reports/${scanId}`, 'reportName')) {
      return db.getData(`/reports/${scanId}/reportName/`);
    } else {
      return false;
    }
  }

  const siteIssues = (scanId, impact) => {
    try {
      let results = db.getData(`/reports/${scanId}/results/`);
      let numInstances = 0;
      results.forEach((view) => {
        view.violations.forEach(violation => {
          if (impact === "all") { numInstances += violation.nodes.length; } else
            if (violation.impact === impact) numInstances += violation.nodes.length;
        });
      });

      return numInstances;
    } catch (e) {
      console.error(e);
      throw Error('Problem getting the site issues')
    }
  }
  // export const rulesViolated = function(impact) {
  //   let numInstances = 0;
  //   report.forEach((view) => {
  //     view.violations.forEach(violation => {
  //       if (impact === "all") { numInstances += violation.nodes.length; } else
  //       if (violation.impact === impact) numInstances += violation.nodes.length;
  //     });
  //   });

  //   return {
  //     critical: siteViolations(critical),
  //     serious: siteViolations(serious),
  //     moderate: siteViolations(moderate),
  //     minor: siteViolations(minor)
  //   };
  // }

  const siteViolations = (scanId, impact) => {
    try {
      let results = db.getData(`/reports/${scanId}/results/`);
      let numViolations = 0;
      results.forEach((view) => {
        view.violations.forEach(violation => {
          if (impact === "all") { numViolations++; } else
            if (violation.impact === impact) { numViolations++; }
        })
      });

      return numViolations;
    } catch (e) {
      console.error(e);
      throw Error('Problem getting the site violations')
    }
  }

  const wcag = scanId => {
    try {
      let results = db.getData(`/reports/${scanId}/results/`);
      let scString = fs.readFileSync('./public/wcag.json', 'utf-8');
      let sc = JSON.parse(scString);
      let passed = 0;
      let categories = [];
      let data = [];
      results.forEach(view => {
        let url = view.url;
        view.violations.forEach(violation => {
          violation.tags.forEach(tag => {
            if (sc[tag] != undefined) {
              if (sc[tag]['tags'].indexOf(url) === -1) sc[tag]['tags'].push(url);
            }
          })
        })
        if (view.violations.length !== 0) passed++;
      });

      for (const [key, value] of Object.entries(sc)) {
        if (value.tags.length !== 0) {
          categories.push(key);
          data.push(value.tags.length);
        }
      }
      let criteria = JSON.parse(scString);
      return {
        categories: categories,
        data: data,
        criteria: criteria
      }

    } catch (e) {
      console.error(e);
      throw Error('Problem getting WCAG failures');
    }
  }
  const getSiteStats = scanId => {
    try {
      let numIssues = siteIssues(scanId, "all");
      let numViolations = siteViolations(scanId, "all");
      let multiplier = (numViolations) ? Math.round((numIssues / numViolations) + "e+2") / 100 : 0;
      return {
        time: formatTimecode(scanId),
        numViews: numCompleted(scanId),
        viewsPassed: viewsPassed(scanId),
        siteHealth: siteHealth(scanId),
        numViolations: numViolations,
        numIssues: numIssues,
        siteMultiplier: multiplier,
        wcag: wcag(scanId)
      };
    } catch (e) {
      console.error(e);
      throw Error('Problem getting site stats');
    }
  }

  const getViewViolationLength = (scanId, viewNum, impact) => {
    try {
      let violations = db.getData(`/reports/${scanId}/results[${viewNum}]/violations/`);
      let all = {
        critical: 0,
        serious: 0,
        moderate: 0,
        minor: 0
      }
      violations.forEach(item => {
        all[item.impact]++
      });

      if (impact === "all") {
        return all;
      } else {
        return {
          i: all[impact]
        }
      }
    } catch (e) {
      console.error(e);
      throw Error('Problem getting the number of view violations')
    }
  }

  const getViewViolationTotal = (scanId, viewNum) => {
    try {
      let { critical, serious, moderate, minor } = getViewViolationLength(scanId, viewNum, "all");
      let i = critical + serious + moderate + minor;
      return { i: i };
    } catch (e) {
      console.error(e);
      throw Error('Problem getting the view violation total')
    }
  }

  const getViewViolationInstancesLength = (scanId, viewNum, impact) => {
    try {
      let violations = db.getData(`/reports/${scanId}/results[${viewNum}]/violations/`);
      let all = {
        critical: 0,
        serious: 0,
        moderate: 0,
        minor: 0
      }
      violations.forEach(violation => {
        all[violation.impact] += violation.nodes.length;
      });

      if (impact === "all") {
        return all;
      } else {
        return {
          i: all[impact]
        }
      }
    } catch (e) {
      console.error(e);
      throw Error('Problem getting the number of instances of view violations')
    }
  }
  const getViewPassesTotal = (scanId, viewNum) => {
    try {
      let passes = db.getData(`/reports/${scanId}/results[${viewNum}]/passes/`).length;
      return {
        i: passes
      }
    } catch (e) {
      console.error(e);
      throw Error('Problem getting the number of rules passed for a view')
    }
  }

  const getViewViolationInstancesTotal = (scanId, viewNum) => {
    try {
      let { critical, serious, moderate, minor } = getViewViolationInstancesLength(scanId, viewNum, "all");
      let i = critical + serious + moderate + minor;
      return { i: i };
    } catch (e) {
      console.error(e);
      throw Error('Problem getting the total number of instances of view violations')
    }
  }
  const getURL = (scanId, viewNum) => {
    try {
      let url = db.getData(`/reports/${scanId}/results[${viewNum}]/url/`);
      //let url = results[viewNum].url;
      return {
        url: url
      }
    } catch (e) {
      console.error(e);
      throw Error('Problem getting the URL for the view')
    }
  }
  const getTitle = (scanId, viewNum) => {
    try {
      let title = db.getData(`/reports/${scanId}/results[${viewNum}]/title/`);
      title = title || getURL(scanId, viewNum);
      return {
        title: title
      }
    } catch (e) {
      console.error(e);
      throw Error('Problem getting the title for the view')
    }
  }

  /* violations for a particular view. */
  const getViolations = (scanId, viewNum) => {
    try {
      let violations = db.getData(`/reports/${scanId}/results[${viewNum}]/violations/`);
      violations.sort((a, b) => (impact.indexOf(a.impact) > impact.indexOf(b.impact)) ? 1 : -1);
      return violations;// results[viewNum].violations;
    } catch (e) {
      console.error(e);
      throw Error('Problem getting the violations for a view')
    }
  }
  const getViewStats = (scanId, viewNum) => {
    try {
      let url = getURL(scanId, viewNum).url;
      let title = getTitle(scanId, viewNum).title;
      let numViolations = getViewViolationTotal(scanId, viewNum).i;
      let numInstances = getViewViolationInstancesTotal(scanId, viewNum).i
      let numPasses = getViewPassesTotal(scanId, viewNum).i;
      let multiplier = Math.round((numInstances / numViolations) + "e+2") / 100;
      let score = Math.round(numPasses / (numViolations + numPasses) * 100);
      return {
        url: url,
        title: title,
        numViolations: numViolations,
        numPasses: numPasses,
        numInstances: numInstances,
        multiplier: multiplier,
        score: score
      }
    } catch (e) {
      console.error(e);
      throw Error('Problem getting the stats for a view')
    }
  }

  const getHead = (scanId, i) => {
    //try {
      let result = db.getData(`/reports/${scanId}/results[${i}]`);
      let { violations, passes, view, url, testEnvironment } = result;
      let numViolations = violations.length;
      let numPasses = passes.length;

      view = (view !== undefined) ? view + ' ' : url + ' ';
      score = Math.round(numPasses / (numViolations + numPasses) * 100);
      windowSize = testEnvironment.windowWidth + 'x' + testEnvironment.windowHeight;
      //i = index;
      return {
        i, view, windowSize, score
      }
    // } catch (e) {
    //   console.error(e);
    //   throw Error('Problem getting page head data')
    // }
  }
  const getHeads = (scanId) => {
    try {
      let n = numViews(scanId);
      let heads = []
      for (i = 0; i < n; i++) {
        let result = db.getData(`/reports/${scanId}/results[${i}]`);
        if (typeof result.error === 'undefined') {
          heads.push(getHead(scanId, i));
        }
      }
      return heads;
    } catch (e) {
      console.error(e);
      throw Error('Problem getting page heads')
    }

  }

  return {
    //initialize: initialize,
    getCheckedTags: getCheckedTags,
    getCheckedDevices: getCheckedDevices,
    getCheckedCategories: getCheckedCategories,
    keyExists: keyExists,
    siteHealth: siteHealth,
    getReportName: getReportName,
    getHead: getHead,
    getHeads: getHeads,
    getViewViolationLength: getViewViolationLength,
    getViewViolationInstancesLength: getViewViolationInstancesLength,
    getViewStats: getViewStats,
    getViolations: getViolations,
    getSiteStats: getSiteStats,
    wcag: wcag
  }
}
