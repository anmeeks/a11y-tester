module.exports = (io, db) => {
    const { combine } = require('./utilities');
    const { a11y } = require('./a11y');
    const reporter = require('./reporter')(db);
    const processor = new (require('./socket-processor'))(db);
    
    io.on("connection", socket => {
        console.log("Made socket connection");
        console.log(socket.id);

        const getPageHead = async runId => {
            try {
                if (!processor.scanCancelled(runId)) {
                    let active = processor.shiftActive(runId);
                    
                    if (active) {
                        try {
                            let numRemaining = processor.numRemaining(runId);
                            io.emit("updateProgress", {runId: runId, numRemaining: numRemaining});
                            let head = await processor.getPageHead(active);
                            if (typeof head.error !== 'undefined') {
                                socket.emit("error", { msg: head.error });
                            } else {
                                head.numRemaining = numRemaining;
                                socket.emit("displayPageHead", head);
                            }
                        } finally {
                            getPageHead(runId);
                        }
                    } else {
                        io.emit("reportFinished", runId);
                        db.delete(`/active/${runId}`);
                        db.push(`/reports/${runId}/status`, 'finished');
                        socket.emit("setCumulative", processor.getCumulative(runId));
                    }
                } else {
                    db.delete(`/cancelled/${runId}`);
                }
            } catch (e) {
                socket.emit("error", { msg: e.message });
            }
        }

        socket.on("getViewViolationLength", async ({ scanId, viewNum }) => {
            try {
                let violations = reporter.getViewViolationLength(scanId, viewNum, 'all');
                socket.emit("viewViolationLength", { violations: violations, viewNum: viewNum });
            } catch (e) {
                console.error(e);
                socket.emit("error", { event: "getViewViolationLength", details: { scanId: scanId, viewNum: viewNum }, error: e.message });
            }
        });

        socket.on("getViewViolationInstancesLength", async ({ scanId, viewNum }) => {
            try {
                let instances = reporter.getViewViolationInstancesLength(scanId, viewNum, 'all');
                socket.emit("viewViolationInstancesLength", { instances: instances, viewNum: viewNum });
            } catch (e) {
                console.error(e);
                socket.emit("error", { event: "getViewViolationInstancesLength", details: { scanId: scanId, viewNum: viewNum }, error: e.message });
            }
        });

        socket.on("getViewStats", async ({ scanId, viewNum }) => {
            let stats = processor.getViewStats({ id: scanId, viewNum: viewNum }) //getViewStats({ id:scanId, viewNum:viewNum });
            socket.emit("viewStats", stats);
        });

        socket.on("getSiteStats", async ({ scanId }) => {
            let stats = reporter.getSiteStats(scanId);
            socket.emit("siteStats", stats);
        });

        socket.on("getViolations", async ({ scanId, viewNum }) => {
            let violations = reporter.getViolations(scanId, viewNum);
            socket.emit("violations", { violations: violations, viewNum: viewNum })
        });
        socket.on("startScanning", async ({ scanId }) => {
            let data = processor.startScanning({ scanId: scanId }); //startScanning({ scanId:scanId, data:data, db:db });
            socket.emit("displayConfig", data);
            io.emit("startScanning", data.runId);
        });
        socket.on("checkGetPageHead", async data => {
            socket.emit("validateGetPageHead", data);
        });
        
        socket.on("removeFromCancelled", async scanId => {
            processor.removeFromCancelled(scanId);// removeFromCancelled({ id:scanId, db:db });
        });
        socket.on("setStatus", async ({ scanId, status }) => {
            db.push(`/reports/${scanId}/status`, status);
            io.emit("statusSet", { scanId: scanId, status: status });
        });
        socket.on("cancelScan", async scanId => {
            processor.cancelScan(scanId);// cancelScan({ id:scanId, db:db });
            io.emit("cancelScan", scanId);
        });
        socket.on("getCumulative", async scanId => {
            socket.emit("setCumulative", processor.getCumulative(scanId));
        });
        socket.on("reportFinished", async scanId => {
            console.log("reportFinished")
            io.emit("reportFinished", scanId);
        });

        socket.on("scanSite", async ({ scanId, runId }) => {
            try {
                let head = await processor.scanSite({ scanId: scanId, runId: runId });
                if (head) socket.emit("displayPageHead", head);
                io.emit("updateProgress", {runId: runId, numRemaining: head.numRemaining});
                getPageHead(runId);

            } catch (e) {
                socket.emit("error", { msg: e.message });
            }

        });

        socket.on("disconnect", () => {
            console.log("DISCONNECTING");
            io.emit("user disconnected", socket.id);
        });
    })
}