const colors = require('colors');

const link = colors.underline.blue;
const pageCol = colors.yellow.bold;
const viewportCol = colors.yellow;
const error = colors.red.bold;
const finished = colors.green.bold;

const selectorToString = (selectors, separator) => {
    separator = separator || ' ';
    return selectors
        .reduce((prev, curr) => prev.concat(curr), [])
        .join(separator);
};

const cliReporter = (...args) => {
    console.log(...args);
};

const logResults = (results) => {
    const { violations } = results;

    if (violations.length === 0) {
        cliReporter(colors.green('  0 violations found!'));
        return;
    }

    const issueCount = violations.reduce((count, violation) => {
        cliReporter(
            '\n' +
            error('  Violation of %j with %d occurrences!\n') +
            '    %s. Correct invalid elements at:\n' +
            violation.nodes
                .map(node => '     - ' + selectorToString(node.target) + '\n')
                .join('') +
            '    For details, see: %s',
            violation.id,
            violation.nodes.length,
            violation.description,
            link(violation.helpUrl.split('?')[0])
        );
        return count + violation.nodes.length;
    }, 0);

    cliReporter(error('\n%d Accessibility issues detected.\n'), issueCount);
}
exports.link = link;
exports.pageCol = pageCol;
exports.viewportCol = viewportCol;
exports.error = error;
exports.finished = finished;
exports.selectorToString = selectorToString;
exports.cliReporter = cliReporter;
exports.logResults = logResults;
