let baseDB = `${__dirname}/../test/db.json`;
let getConfigDB = `${__dirname}/../results/test/getConfig.json`;
let scanCancelledDB = `${__dirname}/../results/test/scanCancelled.json`;
let shiftActiveDB = `${__dirname}/../results/test/shiftActive.json`;
let getRunConfigDB = `${__dirname}/../results/test/getRunConfig.json`;
let prepareActiveDB = `${__dirname}/../results/test/prepareActive.json`;
let scanSiteDB = `${__dirname}/../results/test/scanSite.json`;

describe('getConfig', () => {
    const fs = require('fs');
    const { JsonDB } = require('node-json-db');
    const { Config } = require('node-json-db/dist/lib/JsonDBConfig');
    let db, processor, scan, scanConfig;
    beforeAll(async () => {
        scan = {
            "reportName": "Smaller Scan Edited",
            "devices": [ "desktop", "ipadl", "iphone5" ],
            "tags": [ "wcag2a", "wcag2aa" ],
            "categories": [],
            "urls": [
                "https://www.test.assist.vt.edu/demos/wai/bad/home.html",
                "https://www.test.assist.vt.edu/demos/wai/bad/news.html"
            ],
            "rules": [],
            "lastRun": 1614623314634
        }
        scanConfig = {
            devices: ["Desktop", "iPad landscape", "iPhone 5"],
            tags: ["WCAG 2.0 Level A", "WCAG 2.0 Level AA"],
            categories: []
        }

        fs.copyFileSync(baseDB, getConfigDB);
        db = new JsonDB(new Config(getConfigDB, true, true, '/'));
        processor = new (require('./socket-processor'))(db);
    });

    test('devices and tags but no categories', () => {
        let config = processor.getConfig(scan);
        expect(config.devices).toEqual(scanConfig.devices);
        expect(config.tags).toEqual(scanConfig.tags);
        expect(config.categories).toEqual(scanConfig.categories);
    });

    afterAll(async () => {
        fs.unlinkSync(getConfigDB);
    });
});

describe('scanCancelled', () => {
    const fs = require('fs');
    const { JsonDB } = require('node-json-db');
    const { Config } = require('node-json-db/dist/lib/JsonDBConfig');
    let db, processor;
    beforeAll(async () => {
        fs.copyFileSync(baseDB, scanCancelledDB);
        db = new JsonDB(new Config(scanCancelledDB, true, true, '/'));
        processor = new (require('./socket-processor'))(db);
    });

    test('numerical id is true in cancelled', () => {
        expect(processor.scanCancelled(1614876928535)).toBe(true);
    });

    test('string id is true in cancelled', () => {
        expect(processor.scanCancelled("1614876928535")).toBe(true);
    });

    test('numerical id is not in cancelled', () => {
        expect(processor.scanCancelled(123)).toBe(false);
    });

    afterAll(async () => {
        fs.unlinkSync(scanCancelledDB);
    });
});

describe('shiftActive', () => {
    const fs = require('fs');
    const { JsonDB } = require('node-json-db');
    const { Config } = require('node-json-db/dist/lib/JsonDBConfig');
    let db, processor, scan, config, firstRecord, secondRecord;
    beforeAll(async () => {
        fs.copyFileSync(baseDB, shiftActiveDB);
        db = new JsonDB(new Config(shiftActiveDB, true, true, '/'));
        processor = new (require('./socket-processor'))(db);
        firstRecord = { "i": 0, "id": 1614738740021, "url": "https://vt.edu", "device": "Desktop", "tags": ["wcag2a", "wcag2aa"] };
        secondRecord = { "i": 1, "id": 1614738740021, "url": "https://vt.edu", "device": "iPad landscape", "tags": ["wcag2a", "wcag2aa"] };
    });

    test('shift an object off of currently running scan', () => {
        expect(processor.shiftActive("12345")).toEqual(firstRecord);
        let activeObject = db.getData(`/active/12345`);
        expect(activeObject.length).toBe(5);
        let top = db.getData(`/active/12345[0]`);
        expect(top).toEqual(secondRecord);
    });

    afterAll(async () => {
        fs.unlinkSync(shiftActiveDB);
    });
});

describe('getRunConfig', () => {
    const fs = require('fs');
    const { JsonDB } = require('node-json-db');
    const { Config } = require('node-json-db/dist/lib/JsonDBConfig');
    let db, processor, scan, scanConfig;
    beforeAll(async () => {
        fs.copyFileSync(baseDB, getRunConfigDB);
        db = new JsonDB(new Config(getRunConfigDB, true, true, '/'));
        processor = new (require('./socket-processor'))(db);
        scanConfig = {
            "id": "1614366262667",
            "reportName": "Smaller Scan Edited",
            "devices": [ "Desktop", "iPad landscape", "iPhone 5" ],
            "tags": [ "wcag2a", "wcag2aa" ],
            "urls": [
                "https://www.test.assist.vt.edu/demos/wai/bad/home.html",
                "https://www.test.assist.vt.edu/demos/wai/bad/news.html"
            ]
        }
    });

    test('get the run configuration with valid scanId', () => {
        expect(processor.getRunConfig("1614366262667")).toEqual(scanConfig);
    });

    afterAll(async () => {
        fs.unlinkSync(getRunConfigDB);
    });
});


describe('scanCancelled', () => {
    const fs = require('fs');
    const { JsonDB } = require('node-json-db');
    const { Config } = require('node-json-db/dist/lib/JsonDBConfig');
    let db, processor;
    beforeAll(async () => {
        fs.copyFileSync(baseDB, scanCancelledDB);
        db = new JsonDB(new Config(scanCancelledDB, true, true, '/'));
        processor = new (require('./socket-processor'))(db);
    });

    test('numerical id is true in cancelled', () => {
        expect(processor.scanCancelled(1614876928535)).toBe(true);
    });

    test('string id is true in cancelled', () => {
        expect(processor.scanCancelled("1614876928535")).toBe(true);
    });

    test('numerical id is not in cancelled', () => {
        expect(processor.scanCancelled(123)).toBe(false);
    });

    afterAll(async () => {
        fs.unlinkSync(scanCancelledDB);
    });
});

describe('prepareActive', () => {
    const fs = require('fs');
    const { JsonDB } = require('node-json-db');
    const { Config } = require('node-json-db/dist/lib/JsonDBConfig');
    let db, processor, scanConfig, activeMatch;
    beforeAll(async () => {
        fs.copyFileSync(baseDB, prepareActiveDB);
        db = new JsonDB(new Config(prepareActiveDB, true, true, '/'));
        processor = new (require('./socket-processor'))(db);

        scanConfig = {
            "id": "1614366262667",
            "reportName": "Smaller Scan Edited",
            "devices": [ "Desktop", "iPad landscape", "iPhone 5" ],
            "tags": [ "wcag2a", "wcag2aa" ],
            "urls": [
                "https://www.test.assist.vt.edu/demos/wai/bad/home.html",
                "https://www.test.assist.vt.edu/demos/wai/bad/news.html"
            ]
        }
        activeMatch = [
            { "i": 0, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/home.html", "device": "Desktop", "tags": ["wcag2a", "wcag2aa"] },
            { "i": 1, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/home.html", "device": "iPad landscape", "tags": ["wcag2a", "wcag2aa"] },
            { "i": 2, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/home.html", "device": "iPhone 5", "tags": ["wcag2a", "wcag2aa"] },
            { "i": 3, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/news.html", "device": "Desktop", "tags": ["wcag2a", "wcag2aa"] },
            { "i": 4, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/news.html", "device": "iPad landscape", "tags": ["wcag2a", "wcag2aa"] },
            { "i": 5, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/news.html", "device": "iPhone 5", "tags": ["wcag2a", "wcag2aa"] }
        ]
    });

    test('prepare to start a run of scan 1614366262667', () => {
        let runId = processor.prepareActive({scanId: 1614366262667, runId: 1111});
        expect(runId).toBe(1111);
        let activeObject = db.getData(`/active/${runId}`);
        expect(activeObject.length).toBe(6)
        expect(activeObject).toEqual(activeMatch);
    });

    afterAll(async () => {
        fs.unlinkSync(prepareActiveDB);
    });
});

// describe('scanSite', () => {
//     const fs = require('fs');
//     const { JsonDB } = require('node-json-db');
//     const { Config } = require('node-json-db/dist/lib/JsonDBConfig');
//     let db, processor, scanConfig, activeMatch;
//     beforeAll(async () => {
//         fs.copyFileSync(baseDB, scanSiteDB);
//         db = new JsonDB(new Config(scanSiteDB, true, true, '/'));
//         processor = new (require('./socket-processor'))(db);

//         scanConfig = {
//             "id": "1614366262667",
//             "reportName": "Smaller Scan Edited",
//             "devices": [ "Desktop", "iPad landscape", "iPhone 5" ],
//             "tags": [ "wcag2a", "wcag2aa" ],
//             "urls": [
//                 "https://www.test.assist.vt.edu/demos/wai/bad/home.html",
//                 "https://www.test.assist.vt.edu/demos/wai/bad/news.html"
//             ]
//         }
//         activeMatch = [
//             { "i": 0, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/home.html", "device": "Desktop", "tags": ["wcag2a", "wcag2aa"] },
//             { "i": 1, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/home.html", "device": "iPad landscape", "tags": ["wcag2a", "wcag2aa"] },
//             { "i": 2, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/home.html", "device": "iPhone 5", "tags": ["wcag2a", "wcag2aa"] },
//             { "i": 3, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/news.html", "device": "Desktop", "tags": ["wcag2a", "wcag2aa"] },
//             { "i": 4, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/news.html", "device": "iPad landscape", "tags": ["wcag2a", "wcag2aa"] },
//             { "i": 5, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/news.html", "device": "iPhone 5", "tags": ["wcag2a", "wcag2aa"] }
//         ]
//     });

//     test('start scan with scanId 1614366262667', () => {
//         let head = processor.scanSite(1614366262667);
//     });

//     afterAll(async () => {
//         fs.unlinkSync(scanSiteDB);
//     });
// });
// describe('scanSite', ({ scanId, runId }) => {
//     const fs = require('fs');
//     const { JsonDB } = require('node-json-db');
//     const { Config } = require('node-json-db/dist/lib/JsonDBConfig');
//     let db, processor, scanConfig, activeMatch;
//     beforeAll(async () => {
//         fs.copyFileSync(baseDB, scanSiteDB);
//         db = new JsonDB(new Config(scanSiteDB, true, true, '/'));
//         processor = new (require('./socket-processor'))(db);

//         scanConfig = {
//             "id": "1614366262667",
//             "reportName": "Smaller Scan Edited",
//             "devices": [ "Desktop", "iPad landscape", "iPhone 5" ],
//             "tags": [ "wcag2a", "wcag2aa" ],
//             "urls": [
//                 "https://www.test.assist.vt.edu/demos/wai/bad/home.html",
//                 "https://www.test.assist.vt.edu/demos/wai/bad/news.html"
//             ]
//         }
//         activeMatch = [
//             { "i": 0, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/home.html", "device": "Desktop", "tags": ["wcag2a", "wcag2aa"] },
//             { "i": 1, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/home.html", "device": "iPad landscape", "tags": ["wcag2a", "wcag2aa"] },
//             { "i": 2, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/home.html", "device": "iPhone 5", "tags": ["wcag2a", "wcag2aa"] },
//             { "i": 3, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/news.html", "device": "Desktop", "tags": ["wcag2a", "wcag2aa"] },
//             { "i": 4, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/news.html", "device": "iPad landscape", "tags": ["wcag2a", "wcag2aa"] },
//             { "i": 5, "id": 1614366262667, "url": "https://www.test.assist.vt.edu/demos/wai/bad/news.html", "device": "iPhone 5", "tags": ["wcag2a", "wcag2aa"] }
//         ]
//     });

//     test('start scan with scanId 1614366262667', () => {
//         let head = processor.scanSite(1614366262667);
//     });

//     afterAll(async () => {
//         fs.unlinkSync(scanSiteDB);
//     });
// });