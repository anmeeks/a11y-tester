module.exports = (db) => {
    var express = require('express');
    var router = express.Router();
    const formatTimecode = require('../lib/timecode');
    const axe = require('axe-core');
    const { filterChecked, newId } = require('../lib/utilities');

    let devices, tags, categories;
    devices = db.getData('/constants/devices/');
    tags = db.getData('/constants/tags/');
    categories = db.getData('/constants/categories/');

    const addChecked = (checked, ary) => {
        return ary.map(item => {
            if (checked.indexOf(item.id) !== -1) {
                return {...item, checked: "checked" };
            }
            return item;
        });
    }

    let parseScans = () => {
        let scans = db.getData('/scans/');
        let parsed = [];
        for (const [key, value] of Object.entries(scans)) {
            let lastRun = (value.lastRun) ? formatTimecode(parseInt(value.lastRun, 10)) : "Not Run Yet";
            parsed.push({
                id: key,
                reportName: value.reportName,
                lastRun: lastRun
            })
        }
        return parsed;
    }
    const renderIndex = msg => {
        let scans = db.getData('/scans/');
        let data = {
            title: 'Scan Configurations',
            scans: parseScans(scans),
            breadcrumbs: {
                targets: [{
                        url: '/',
                        text: 'Home'
                    },
                    {
                        text: 'Scan Configurations'
                    }
                ]
            }
        }
        if (msg) data.msg = msg;
        return data;
    }

    router.get('/', function(req, res, next) {
        let data = renderIndex(((typeof req.query.status !== "undefined") ? "Scan Added" : null));
        //if (req.query.status === added) 
        res.render('scans/index', data);
    });

    router.get('/scan/create', function(req, res, next) {
        // TODO: Add ability to pull in sitemaps
        // https://bytenota.com/parsing-an-xml-sitemap-in-javascript/
        res.render('scans/scan', {
            title: `Create a Scan Configuration`,
            breadcrumbs: {
                targets: [{
                        url: '/',
                        text: 'Home'
                    },
                    {
                        url: '/scans',
                        text: 'Scan Configurations'
                    },
                    {
                        text: `Create`
                    }
                ]
            },
            urlString: "[]",
            devices: addChecked(["desktop", "ipadl", "iphone5"], devices),
            tags: addChecked(['wcag2a', "wcag2aa"], tags),
            categories: addChecked([], categories),
            action: "/scans/scan/create"
        });
    });

    router.post('/scan/create', (req, res, next) => {
        var d = new Date();
        let id = d.getTime();
        let conf = {
            reportName: req.body.reportName,
            urls: JSON.parse(req.body.urls).urls,
            devices: req.body.devices || [],
            tags: req.body.tags || [],
            categories: req.body.categories || [],
            rules: [],
            lastRun: null
        }
        db.push(`/scans/${id}/`, conf);
        res.redirect("/scans/?status=added");

    })

    router.get('/scan/update/:id', function(req, res, next) {
        let id = req.params.id;
        let checkedDevices = db.getData(`/scans/${id}/devices`);
        let checkedTags = db.getData(`/scans/${id}/tags`);
        let checkedCategories = db.getData(`/scans/${id}/categories`);
        let urls = db.getData(`/scans/${id}/urls`);
        let urlString = JSON.stringify(urls);
        res.render('scans/scan', {
            title: `Edit Scan Configuration`,
            breadcrumbs: {
                targets: [{
                        url: '/',
                        text: 'Home'
                    },
                    {
                        url: '/scans',
                        text: 'Scan Configurations'
                    },
                    {
                        text: `Edit`
                    }
                ]
            },
            reportName: db.getData(`/scans/${id}/reportName`),
            id: id,
            urlString: urlString,
            devices: addChecked(checkedDevices, devices),
            tags: addChecked(checkedTags, tags),
            categories: addChecked(checkedCategories, categories),
            rules: axe.getRules(),
            lastRun: db.getData(`/scans/${id}/lastRun`),
            action: "/scans/scan/update"
        });
    });
    router.post('/scan/update', function(req, res, next) {
        let id = req.body.id;

        let urls = JSON.parse(req.body.urls).urls;
        let urlString = JSON.stringify(urls);

        let conf = {
            reportName: req.body.reportName,
            devices: req.body.devices || [],
            tags: req.body.tags || [],
            categories: req.body.categories || [],
            urls: urls,
            rules: [],
            lastRun: db.getData(`/scans/${id}/lastRun`)
        }
        db.push(`/scans/${id}/`, conf);
        let data = {
                title: `Edit Scan Configuration`,
                breadcrumbs: {
                    targets: [{
                            url: '/',
                            text: 'Home'
                        },
                        {
                            url: '/scans',
                            text: 'Scan Configurations'
                        },
                        {
                            text: `Edit`
                        }
                    ]
                },
                reportName: conf.reportName,
                id: id,
                urlString: urlString,
                devices: addChecked(conf.devices, devices),
                tags: addChecked(conf.tags, tags),
                categories: addChecked(conf.categories, categories),
                //rules: axe.getRules(),
                //lastRun: conf.lastRun,
                msg: "Successfully Saved"
            }
            //console.log(data);
        res.render('scans/scan', data);
    });

    router.get('/scan/delete/:id', function(req, res, next) {
        db.delete('/scans/' + req.params.id);
        res.json({ id: req.params.id });
    });

    router.get('/scan/duplicate/:id', function(req, res, next) {
        let id = req.params.id;
        let source = db.getData(`/scans/${id}`);
        //console.log(source);
        //let iterator = Object.entries(source);
        let destId = newId();
        let destName = `${source.reportName} (copy)`;
        let dest = {};
        for (const [key, value] of Object.entries(source)) {
            dest[key] = (Array.isArray(value)) ? [...value] : value;
        }
        dest.reportName = destName;
        dest.lastRun = null;
        console.log(dest);

        db.push(`/scans/${destId}`, dest);
        res.json({ id: destId, reportName: destName });
    });
    router.get('/scan/run/:id', function(req, res, next) {
        let scan = db.getData('/scans/' + req.params.id);
        let withTags = [
            ...filterChecked({ checked: scan.tags, ary: tags, key: 'id' }),
            ...filterChecked({ checked: scan.categories, ary: categories, key: 'id' })
        ]
        let parsed = {
            id: req.params.id,
            urls: scan.urls,
            devices: filterChecked({ checked: scan.devices, ary: devices, key: 'description' }),
            tags: withTags,
            //tags: getIds(scan.tags, tags),
            //categories: getIds(scan.categories, categories),
            reportName: scan.reportName
        }
        let data = JSON.stringify(parsed);
        res.render('scans/run', {
            title: `Run Scan Config: ${scan.reportName}`,
            breadcrumbs: {
                targets: [{
                        url: '/',
                        text: 'Home'
                    },
                    {
                        url: '/scans',
                        text: 'Scan Configurations'
                    },
                    {
                        text: `Run`
                    }
                ]
            },
            reportName: scan.reportName,
            id: req.params.id,
            urls: scan.urls,
            devices: filterChecked({ checked: scan.devices, ary: devices, key: 'description' }),
            tags: filterChecked({ checked: scan.tags, ary: tags, key: 'id' }),
            categories: filterChecked({ checked: scan.categories, ary: categories, key: 'id' }),
            data: data
        });
    })
    router.get('/sitemap', function(req, res, next) {

        res.render('scans/sitemap', {
            title: `Convert Sitemap File to JSON`,
            breadcrumbs: {
                targets: [{
                        url: '/',
                        text: 'Home'
                    },
                    {
                        url: '/scans',
                        text: 'Scan Configurations'
                    },
                    {
                        text: `Convert Sitemap`
                    }
                ]
            }
        });
    })

    router.get('/scan/read/:id', function(req, res, next) {
        let scan = db.getData('/scans/' + req.params.id);
        let parsed = {
            urls: scan.urls,
            devices: filterChecked({ checked: scan.devices, ary: devices, key: 'description' }),
            tags: filterChecked({ checked: scan.tags, ary: tags, key: 'id' }),
            categories: filterChecked({ checked: scan.categories, ary: categories, key: 'id' }),
            reportName: scan.reportName
        }
        res.json({ conf: parsed });
    });
    return router;
};