

module.exports = (db) => {
  var express = require('express');
  var router = express.Router();
  const reporter = require('../lib/reporter')(db);
  //const { test } = require('../lib/test');
  const getTimecode = require('../lib/timecode');

  const getReports = () => {
    let results = db.getData('/reports/');
    let reports = {
      finished: [],
      processing: []
    }
    for (const [key, value] of Object.entries(results)) {
      let status = value.status;
      if (status === "finished") {
        let siteHealth = reporter.siteHealth(key);
        reports.finished.push({
          id: key,
          time: getTimecode(parseInt(key, 10)),
          reportName: value.reportName || "Not Provided",
          siteHealth: siteHealth
        })
      } else if (status === "processing") {
        reports.processing.push({
          id: key,
          time: getTimecode(parseInt(key, 10)),
          reportName: value.reportName || "Not Provided"
        })
      }
    }
    return reports;
  }

  const getReport = scanId => {
    let report = db.getData(`/reports/${scanId}`);
    return report;
  }

  /* GET home page. */
  router.get('/', function (req, res, next) {
    res.render('reports/index', {
      title: 'List of Reports',
      reports: getReports(),
      breadcrumbs: {
        targets: [
          {
            url: '/',
            text: 'Home'
          },
          {
            text: 'Reports'
          }
        ]
      }
    });
  });
  router.get('/get/:id', function (req, res, next) {
    let scanId = req.params.id;
    let time = getTimecode(parseInt(scanId, 10));
    let reportName = reporter.getReportName(scanId);
    res.render('reports/report', {
      title: `${reportName} (${time})`,
      breadcrumbs: {
        targets: [
          {
            url: '/',
            text: 'Home'
          },
          {
            url: '/reports',
            text: 'Reports'
          },
          {
            text: `Report`
          }
        ]
      },
      time: time,
      reportName: reportName,
      stats: reporter.getSiteStats(scanId),
      devices: reporter.getCheckedDevices(scanId),
      tags: reporter.getCheckedTags(scanId),
      categories: reporter.getCheckedCategories(scanId),
      heads: reporter.getHeads(scanId)
    })
  });
  router.get('/delete/:id', function (req, res, next) {
    db.delete('/reports/' + req.params.id);
    res.json({ id: req.params.id });
  });
  router.get('/cancel/:id', function (req, res, next) {
    db.push(`/reports/${req.params.id}/status`, "cancelled");
    res.json({ id: req.params.id });
  });
  router.get('/wcagFailures/:id', function (req, res, next) {
    res.json(reporter.wcag(req.params.id));
  });


  router.get('/export/:id', function (req, res, next) {
    res.json(getReport(req.params.id));
  });

  //////////////////////////////////////
  router.get('/row/get/:id', function (req, res, next) {
    let id = req.params.id;
    res.json({
      id: id,
      time: getTimecode(parseInt(id, 10)),
      reportName: reporter.getReportName(id),
      siteHealth: reporter.siteHealth(id)
    });
  });
  return router;
};
